import Vue from 'vue'
export default {
  namespaced: true,
  state: {
    contacts: [
      {
        id: 1,
        name: 'Freddy Mercury',
        'e-mail': 'frontman@queen.en',
      },
      {
        id: 2,
        name: 'Scott Stapp',
      },
      {
        id: 3,
        name: 'David Gilmour',
      },
      {
        id: 4,
        name: 'Mike Ness',
      },
      {
        id: 5,
        name: 'Bono',
      },
    ],
  },
  contactHistory: {},
  mutations: {
    deleteContact(state, { id }) {
      state.contacts = state.contacts.filter((contact) => contact.id !== id)
    },
    createContact(state, contact) {
      contact.id = Date.now()
      state.contacts.push(contact)
    },
    changeField(state, { id, oldKey, key, value }) {
      Vue.delete(
        state.contacts[state.contacts.findIndex((el) => el.id === id)],
        oldKey
      )
      Vue.set(
        state.contacts[state.contacts.findIndex((el) => el.id === id)],
        key,
        value
      )
    },
    deleteField(state, { id, key }) {
      Vue.delete(
        state.contacts[state.contacts.findIndex((el) => el.id === id)],
        key
      )
    },
    addField(state, { id, key, value }) {
      Vue.set(
        state.contacts[state.contacts.findIndex((el) => el.id === id)],
        key,
        value
      )
    },
  },
  actions: {
    deleteContact(context, { id }) {
      context.commit('deleteContact', { id })
    },
    createContact(context, contact) {
      context.commit('createContact', contact)
    },
    changeField(context, { id, oldKey, key, value }) {
      context.commit('changeField', { id, oldKey, key, value })
    },
    deleteField(ctx, { id, key }) {
      ctx.commit('deleteField', { id, key })
    },
    addField(ctx, { id, key, value }) {
      ctx.commit('addField', { id, key, value })
    },
  },
  getters: {
    allContacts: (state) => {
      return state.contacts
    },
    contact: (state) => (id) => {
      return state.contacts.find((el) => el.id === id)
    },
  },
}
